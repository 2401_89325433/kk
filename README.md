# kk

from DrissionPage import ChromiumPage
from DrissionPage.common import Actions
from DrissionPage.errors import ElementNotFoundError
 
class_name = input('课程名称：')
rule_time = 3
seep_time = 2
tab_1 = ChromiumPage().latest_tab
ac = Actions(tab_1)
 
# 进入相应课堂
def Input_class(tab, class_name):
    in_class_s = tab.ele('@id=sharingClassed', timeout=rule_time)
    if in_class_s:
        in_class = in_class_s.ele(f'@text()={class_name}', timeout=rule_time)
        if in_class:
            try:
                in_class.click(timeout=rule_time)
                print(f'成功点击课堂：{class_name}')
                return 1
            except ElementNotFoundError as e:
                print(f'点击课堂时发生错误：{e}')
                return 0
        else:
            print(f'没有找到相应课堂：{class_name}')
            return 0
    else:
        print('没找到共享课堂？')
        return 0
 
# 取消弹出学前必读0.0
def Out_window_0():
    top_nf = tab_1.ele('x://*[@id="app"]/div/div[6]', timeout=rule_time)
    length = len(str(top_nf))
    if length == 64:
        top_out = tab_1.ele('@class=iconfont iconguanbi', timeout=rule_time)
        if top_out:
            try:
                top_out.click(timeout=rule_time)
                print('点击取消学前必读标签页成功！')
                return 1
            except ElementNotFoundError as e:
                print(f'点击取消学前必读标签页错误{e}')
                return 0
        else:
            print('没有找到取消标签页的X号')
            return 0
    else:
        print('无需取消标签页')
        return 0
 
def Watch_video():
    tab_1.ele('x://*[@id="vjs_container"]/div[8]').click()
    print('选择选后成功播放视频')
 
def Watch_video_1():
    tab_1.ele('@class=volumeBox').click()
    tab_1.ele('@class=playButton').click()
    tab_1.ele('@class=speedBox').click()
    tab_1.ele('@text()=X 1.5').click()
    print('点击观看、静音、1.5倍成功')
 
def Tell_pick():
    have = tab_1.ele('@aria-label=弹题测验')
    picks = tab_1.eles('@class=icon topic-option')
    if have:
        a = picks[0]
        if a:
            a.click()
            print('成功引出